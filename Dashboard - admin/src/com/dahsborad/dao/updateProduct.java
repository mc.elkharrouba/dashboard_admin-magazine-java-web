package com.dahsborad.dao;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dashboard.beans.Gateway;
import com.dashboard.beans.Product;

/**
 * Servlet implementation class updateProduct
 */
@WebServlet("/updateProduct")
public class updateProduct extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public updateProduct() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String nom = request.getParameter("nom");
		String id = request.getParameter("id");
		String description = request.getParameter("description");
		String prix= request.getParameter("prix");
		String category_id = request.getParameter("category_id");
		String file = request.getParameter("file");
		if( nom.equals("") && description.equals("") && prix.equals("")  && file.equals("") )
		{
			//response.sendRedirect("addpro?operation=failed");
		}
		else
		{
			
			Product proudct = new Product();
			
			proudct.setId(Integer.parseInt(id));
			proudct.setNom(nom);
			proudct.setDesc(description);
			proudct.setPrix(Float.parseFloat(prix));
			proudct.setImg(file);
			proudct.setIdc(Integer.parseInt(category_id));
			Gateway.updateProduct(proudct);
			response.sendRedirect("products?operation=success_updated");
			
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	}

}
