package com.dashboard.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dashboard.beans.Gateway;
import com.dashboard.beans.Product;

/**
 * Servlet implementation class productsController
 */
@WebServlet("/products")
public class productsController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public productsController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String operation = request.getParameter("operation");
		String message ="";
		if(operation != null)
		{
			if(operation.equals("success_updated"))
			{
				message = "PRODUCT HAS BEEN UPDATED SUCCESSFULLY !";
			}
			else if(operation.equals("success_deleted"))
			{
				message = "PRODUCT HAS BEEN DELETED SUCCESSFULLY !";
			}
			
		}
		ArrayList<Product> list = new ArrayList<Product>();
		list = Gateway.getAllProducts();
		
		request.setAttribute("list", list);
		request.setAttribute("message", message);
		
		RequestDispatcher dispatch = request.getRequestDispatcher("listallproduct.jsp");
		dispatch.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
	}

}
