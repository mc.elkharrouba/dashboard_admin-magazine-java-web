<%@page import="com.dahsborad.dao.Procces"%>
<%@page import="com.dashboard.beans.Product"%>
<%@page import="com.dashboard.beans.Category"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	Procces.notLogin(request, response);
	ArrayList<Product> list = (ArrayList<Product>) request.getAttribute("list");
	String message = (String) request.getAttribute("message");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Dashborad admin</title>
  <link href="https://fonts.googleapis.com/css2?family=Baloo+2&display=swap" rel="stylesheet">
  <script src="https://kit.fontawesome.com/34ccdadeec.js" crossorigin="anonymous"></script>
  <!-- Bootstrap core CSS -->
  <link href="./css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="./css/style.css" rel="stylesheet">
</head>
<body>
	<div class="upper-nav">
								<div class="container">
									<div class="row">
									    <div class="col-sm text-sm-left">
									       <i class="fas fa-phone-alt">&nbsp;</i><span>(432)782-1820</span>&nbsp;&nbsp;&nbsp;
									       <i class="far fa-envelope"></i>&nbsp;<span>mc.elkharrouba@gmail.com</span>
									    </div>
									    <div class="col-sm sm text-sm-right text-right">
									      <span>Sign Up</span>&nbsp;&nbsp;&nbsp;
									      <span class="signin">Sign in</span>
									    </div>
									</div>
								</div>
							</div>
								
								<div class="second-nav">
								<div class="container">
									<nav class="navbar navbar-expand-lg navbar-light ">
									  <a class="navbar-brand" href="${contextRoot}/home">PV<span class="gadget-logo">Gamer</span></a>
									  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
									    <span class="navbar-toggler-icon"></span>
									  </button>
									  <div class="collapse navbar-collapse" id="navbarText">
									     <ul class="navbar-nav ml-auto">
									      <li class="nav-item" id="home">
									        <a class="nav-link" href="${contextRoot}/Dashboard_-_admin/products">Accueil</a>
									      </li>
									      <li class="nav-item" id="shop">
									        <a class="nav-link" href="${contextRoot}/Dashboard_-_admin/addpro">add products</a>
									      </li>

									      <li class="nav-item" id="conatct">
									        <a class="nav-link" href="${contextRoot}/Dashboard_-_admin/logout">Log out </a>
									      </li>
									      <li class="nav-item" >
									        <a class="nav-link" href="${contextRoot}/cart">
									        	<i class="fas fa-shopping-cart cart">
									        	<span class="badge badge-light">0</span></i>
									        </a>
									      </li>
									    </ul>
									  </div>
									</nav>
								</div>
</div>

<div>
	<div class="container">
  			<div class="z-top" style="margin-top: 50px;">
  			
						<h1 class="text-center">Available Products</h1>
					
								<hr>
								<br/>
									<% if( message != ""){ %>
				  					<div class="alert alert-success" role="alert" id="message"><%=message %></div>
				  					<%} %>
								<br/>
								
								<table id="productsTable"  class="table table-condensed table-bordered">
												
									<thead>					
										<tr>					
											<th>Id</th>
											<th>&#160;</th>
											<th>nom</th>
											<th>Description</th>
											<th>Unit Price</th>			
											<th>Edit</th>
											<th>Delete</th>
										</tr>					
									</thead>
									<% for(Product p : list){ %>
										<tr>
										<td><%=p.getId() %></td>
										<td>
											<img  height="100px" width="100px" src="./img/<%=p.getImg()%>">
										
										</td>
										<td>
											<%=p.getNom() %>
										</td>
										<td>
											<%=p.getDesc() %>
										</td>
										<td>
											<%=p.getPrix() %>
										</td>
										
										<td>
											<a href="updatepro?idp=<%=p.getId()%>"><button type="button" class="btn btn-warning">Edit</button></a>
											
										</td>
										<td>
											<a href="deleteProduct?idp=<%=p.getId()%>"><button type="button" class="btn btn-danger">Delete</button></a>
											
										</td>
										</tr>
									<%} %>
									 <tfoot>
										<tr>					
											<th>Id</th>
											<th>&#160;</th>
											<th>Name</th>
											<th>Description</th>
											<th>Unit Price</th>				
											<th>Edit</th>
											<th>Delete</th>
										</tr>									
									</tfoot>
									
									<!-- Modal -->
							
									
									
												
								</table>
		
		


			</div>
	</div>

  <script src="./jquery/jquery.min.js"></script>
  <script src="./js/bootstrap.bundle.min.js"></script>
  <script src="./js/mini.js" ></script>
</body>
</html>