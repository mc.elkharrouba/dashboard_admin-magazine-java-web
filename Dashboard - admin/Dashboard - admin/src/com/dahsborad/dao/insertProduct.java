package com.dahsborad.dao;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Paths;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dashboard.beans.Gateway;
import com.dashboard.beans.Product;

/**
 * Servlet implementation class insertProduct
 */
@WebServlet("/inscprod")
public class insertProduct extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public insertProduct() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String nom = request.getParameter("nom");
		String description = request.getParameter("description");
		String prix= request.getParameter("prix");
		String category_id = request.getParameter("category_id");
		String file = request.getParameter("file");
		
		if( nom.equals("") && description.equals("") && prix.equals("")  && file.equals("") )
		{
			response.sendRedirect("addpro?operation=failed");
		}
		else
		{
		    String uploadDirectory = "/Utilisateurs/hamzaelkharrouba/Bureau/all project/Boutique.zip_expanded/Boutique/WebContent/img";
		   
			System.out.print("uploadDirectory :" + uploadDirectory);
			String FileName = file;
			String FilePath = Paths.get(uploadDirectory, FileName).toString();
			System.out.print("FilePath :" + FilePath);
			
			
			File Dir = new File(uploadDirectory);
			if(!Dir.exists())
			{
				Dir.mkdirs();
			}
			
			try {
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(FilePath)));
				stream.write(file.getBytes());
				stream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			
			Product proudct = new Product();
			
			
			proudct.setNom(nom);
			proudct.setDesc(description);
			proudct.setPrix(Float.parseFloat(prix));
			proudct.setImg(file);
			proudct.setIdc(Integer.parseInt(category_id));
			
			Gateway.insertProduct(proudct);
			
			response.sendRedirect("addpro?operation=sucess");
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
	}

}
