package com.dahsborad.dao;

import java.io.IOException;

import javax.security.auth.message.callback.PrivateKeyCallback.Request;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dashboard.beans.Admin;
import com.dashboard.beans.Gateway;

/**
 * Servlet implementation class Procces
 */
@WebServlet("/procces")
public class Procces extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Procces() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request,response);
		HttpSession session = request.getSession();
		Boolean Login = false;
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		Admin admin = new Admin();
		admin = Gateway.getAdmin(username, password);
		
		if(username.equals(admin.getUsername()) && password.equals(admin.getPassword()) )
		{

			session.setAttribute("username", admin.getUsername());
			session.setAttribute("password", admin.getPassword());

			//RequestDispatcher dispatch = request.getRequestDispatcher("");
			//dispatch.forward(request, response);
			response.sendRedirect("products");
		}else
		{
		
			response.sendRedirect("index.jsp?login=not_valid");
		}


	}
	
	public static void notLogin(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException  
	{
		HttpSession session = request.getSession();
		if(session.getAttribute("username") == null && session.getAttribute("password") == null)
		{
			response.sendRedirect("index.jsp?login=not_valid");
		}
		
	}
	public static void isLogin(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException  
	{
		HttpSession session = request.getSession();
		if(session.getAttribute("username") != null && session.getAttribute("password") != null)
		{
			response.sendRedirect("products");
		}
		
	}
	public static Boolean Login(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException  
	{
		Boolean exsite = false;
		HttpSession session = request.getSession();
		if(session.getAttribute("username") != null && session.getAttribute("password") != null)
		{
			exsite = true;
		}
		 return exsite;
	}

}
